Laboratório Esportivo


Neste laboratório, você irá construir um jogo esportivo simples. Ele irá exercitar seu nível de conforto com o JSX, tanto com componentes React stateful quanto stateless, trabalho com props, e o aprimoramento de estados locais dentro de um componente stateful.

Para começar, você precisa preparar tudo, então poderemos executar nossa aplicação React. Isso inclui a criação de uma página HTML para exibir nossa aplicação, a importação do React e de outros scripts, e a alavancagem desses scripts para renderizar uma aplicação em nossa página HTML.

Preparação
Prepare uma página HTML que irá apresentar a aplicação aos nossos usuários.
Prepare um componente padrão para o nosso app que o  ReactDOM.render() renderize em nossa página HTML. Isso pode ser um componente stateless.
App é um nome comum para esse componente padrão. Nosso App pode ser formado por muitos componentes, mas renderizamos esse componente padrão para exibir toda a aplicação.
O ReactDOM.render() deve renderizar sempre apenas este componente.
Todos os outros componentes serão renderizados através desse componente, seja referenciado diretamente neste componente ou como filho de componentes referenciados diretamente neste componente.
O código neste repo, index.html e o reactApp.js, representam o laboratório até aqui. Fique à vontade para usar este código como um início ou como referência para ajudá-lo a começar.

Modo Normal
Vamos começar a construir nosso jogo.

O Componente Time
Normalmente, em uma partida esportiva, queremos que nosso time vença. Para jogar este jogo, você precisará criar times para que eles possam competir pela vitória. Aqui embaixo há um esboço da funcionalidade que nosso componente Team deve incluir.

Crie um componente Team stateful que aceita um nome e um caminho até um logo para o time em forma de prop.
Este componente deve exibir o nome do time e o logo fornecido através do prop.
O componente Team também deve exibir alguma informação sobre o time.
Ele deve exibir o número de Chutes Realizados.
Ele deve exibir os Pontos do time.
O componente Team deve ter um botão Chutar. Não pontuamos sempre que chutamos, portanto vamos discutir a funcionalidade do botão chutar.
Quando um chute é realizado, o contador Chutes Realizados deve sempre aumentar.
Deve haver alguma chance aleatória de que o contador Pontos aumente.
Certifique-se de renderizar e testar seu componente Team à medida que adiciona novas funcionalidades.
Preparação da nossa batalha de times esportivos
Agora que temos um componente padrão que representa nossa aplicação e criamos nosso componente Team, podemos iniciar a montagem do jogo.

Atualize o componente App padrão que você criou para exibir dois times, o time da casa e o time visitante.
Use seus conhecimentos de HTML e CSS para fazer a página se parecer mais com um jogo onde dois times, o da casa e o visitante, estão se enfrentando em uma competição.
Confirme se o os dois times estão sendo exibidos e mantêm o registro de suas informações.
Modo Intermediário
Vamos adicionar uma funcionalidade extra ao nosso jogo esportivo.

Adicione uma funcionalidade para tocar um som quando um time chuta e um som diferente para quando um time pontua. Os recursos abaixo devem ajudar a completar essa parte:
https://developer.mozilla.org/en-US/docs/Web/API/HTMLAudioElement
https://www.freesoundeffects.com/free-sounds/sports-10098/
Vamos adicionar mais algumas estatísticas aos nossos times. Vamos exibir uma métrica de Aproveitamento. Ela será os chutes pontuados dividos pelos chutes não pontuados.
Esta métrica deve ser exibida apenas se um chute tiver sido realizado. Ela não deve estar visível se o time não tiver tentado um chute ainda.
Crie um componente Game que aceita um nome de local como prop.
Atualize seu componente padrão que está exibindo seus times no momento para, em vez disso, exibir o componente Game. Mova tudo que o componente padrão estiver exibindo para o componente Game
Atualize o componente Game para exibir "Seja bem-vindo" seguido do nome do local passado como prop.
Seu app ainda deve renderizar o componente padrão com ReactDOM.render(), normalmente nomeado como App.
O componente padrão App deve renderizar apenas o componente Game em vez de renderizar os componentes Team como anteriormente.
O componente Game deve exibir o nome do local para o jogo e os componentes Team que foram exibidos anteriormente no componente padrão App.
Cada time deve se comportar como antes, mas agora exibem uma métrica de Aproveitamento condicional se tiver realizado um chute.
Pense no que aconteceria se adicionássemos outro componente Game ao nosso App. Poderíamos mudar o nome do Local mas queremos apenas criar novos jogos com os mesmo times. O que poderíamos modificar nos componentes Game e Team para nos permitir ter times únicos para cada partida que incluirmos no nosso componente padrão App?
Modo Difícil
O modo difícil não é obrigatório e está aqui para desafiá-lo em conceitos relacionados a estado. No modo difícil, queremos modificar o componente Game para ser a fonte da verdade do estado do jogo. O componente Game deve manter um registro de como o jogo é jogado e de eu progresso. No momento, a maioria dessas coisas é registrada em nosso componente Team. Nosso componente Game irá gerenciar dois times, o da casa e o visitante, e suas estatísticas durante a partida. Para concluir essa etapa, iremos converter nosso componente Team em um componente funcional stateless e fazer o componente stateful Game passar os dados aos times em forma de props. Este conceito é chamado de  Lifting State Up. Estamos compartilhando o estado com todos os componentes afetados ao movê-lo para cima para o ancestral comum mais próximo dos componentes que dependem dele.

Converter o componente Team para um componente stateless funcional.
Tudo o que estava dentro do componente Team antes e dependia do estado agora deve depender dos props vindos do componente Game.
Atualize o componente Game para manter o registro das estatísticas dos times da casa e visitante e passe esses dados ao componente Team em forma de props.
Como todos os dados do jogo serão registrados pelo estado do componente Game, o método usado ao clicar no botão chutar de um time também deve ser passado como um prop. Este método precisa afetar o estado do componente correto.
Adicione um botão Reiniciar Jogo ao Game e um contador exibindo o número de reinicializações.
Quando o botão for pressionado, as estatísticas do time devem ser reiniciadas e o contador de reinicializações deve aumentar em 1.
Crédito Extra
Crie um componente Placar que exibe a pontuação dos times CASA e VISITANTE.