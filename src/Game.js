import React from "react";

import "./Game.css";

import Team from "./Team";
import SoccerField from "./SoccerField";

import ashTeam from "./img/ashTeam.png";
import RocketTeam from "./img/rocketTeam.png";

class Game extends React.Component {
  render() {
    return (
      <>
      <h1>Bem vindo a {this.props.local}</h1>
      <div className="granBox">
        <Team name="Ash" logo={ashTeam} />
        <SoccerField />
        <Team name="Rocket Team" logo={RocketTeam} />
      </div>
      </>
    );
  }
}

export default Game;
