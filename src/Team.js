import React from "react";

import "./Team.css";

import pikachuPoint from "./mp3/pikachuPoint.mp3";
import kick from "./mp3/kick.mp3";

class Team extends React.Component {
  state = {
    kicks: 0,
    points: 0,
    shotPercentage: 0,
  };

  //Rola um dado e sempre que o valor for '6' adiciona um gol ao contador
  dice = () => {
    let point = new Audio(pikachuPoint);
    let kickIt = new Audio(kick);
    let dado = Math.ceil(Math.random() * 6);

    console.log("Rolou os dados");
    console.log(dado);

    if (dado === 6) {
      this.setState({ points: this.state.points + 1 });
      point.play();
    } else {
      kickIt.play();
    }
  };

  shotPercentage = () => {
    this.setState({ shotPercentage: this.state.points / this.state.kicks });
    console.log(this.state.shotPercentage);
  };

  //chama a função para rolar o dado e adiciona um chute ao contador
  kick = () => {
    this.dice();
    this.setState({ kicks: this.state.kicks + 1 });
    this.shotPercentage();
  };

  isShotPercentageVisible = () => {
    if (this.state.points === 0) {
      return <></>;
    } else {
      return <h3>Aproveitamento: {this.state.shotPercentage.toFixed(3)}</h3>;
    }
  };

  render() {
    return (
      <div className="soccerCard">
        <img src={this.props.logo} alt={this.props.name} id={this.props.name} />
        <h1>{this.props.name}</h1>
        <h2>Chutes Realizados: {this.state.kicks}</h2>
        <h2>Pontos: {this.state.points}</h2>
        <this.isShotPercentageVisible />
        <button onClick={() => this.kick()}>realizar chute</button>
      </div>
    );
  }
}

export default Team;
